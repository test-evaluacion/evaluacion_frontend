# Reto FrontEnd
Crear una Aplicación Web de mantenimiento (CRUD) de Usuarios usando Nuxt/VueJS.

Requesitos necesarios:
  - Conocimientos JavaScript ES6
  - Conociemientos de Vuejs
  - Conocimientos de Nuxt
  - Concocimientos en consumo de servicios REST.
  - Conocimientos de VUEX

Finalidad del reto, es obtener lo siguiente: :eyes: :eyes:
  - Comprobar conocimientos de JavaScript.
  - Legibilidad de código.
  - Aplicación de conceptos básicos de Vuejs.
  - Uso de Git.
  - Realizar Merge Request(GitLab).
  - Manejo de Vuex a nivel de estados, aciones, mutaciones.

Para el reto se plantearan pasos a seguir, no necesariamente estos pasos se deben seguir al pie de la letra.

  1. Tener una cuenta en Gitlab.

  2. Realizar Fork al repositorio base que contiene el código Nuxt.

  3. Preparar su ambiente para realizar los Merge Request.
  
Una vez haya hecho los pasos anteriores con respecto a Git, nos ponemos manos a la obra. :v: :punch:

  4. Inicializar el proyecto. En caso de no tener conocimientos sólidos de Nuxt leer su documentación ([Nuxt Guide](https://nuxtjs.org/guide)).

  5. Instalar Vuetify.
  
Acá empieza lo divertido. :muscle: :metal:

  6. Crear las siguientes rutas/páginas:
      * Página principal para la bienvenida, esta página simplemente tendrá un texto de bienvenida, este texto debe estar centrado.
      * Página secundaria el cual servirá para listar a los usuarios.
  
  7. Crear menú de navegación para el redireccionamiento a las distintas.

  8. Crear un componente tabla reutilizable para listar los usuarios. Usar Vuetify ([Get started with Vuetify](https://vuetifyjs.com/es-MX/getting-started/quick-start)).

  9. Agregar componente tabla a la página de los usuarios.

  10. Consumir servicio que lista usuarios y almacenarlos en el State de Vuex. Puedes usar el paquete que desees para realizar peticiones HTTP, como por ejemplo: axios o traejs.
    ```
        [https://jsonplaceholder.typicode.com/users]
    ```
  11. El Punto 9 debe ejecutarse al lado del servidor. Ver la documentación de [Nuxt](https://nuxtjs.org/guide) para saber que función realiza dicho propósito.

  12. Crear las funciones: Listar, agregar, editar y eliminar.

* Función Listar: :memo:
  1. Listar usuarios en la tabla, para ello usar vuex para almacenar los datos del servicio. Puedes usar PROPS o Computed Properties. En relación con el Punto 9.
  2. Al listar los usuario, cada registro debe contar con los botones: editar y eliminar.

* Función Agregar: :heavy_plus_sign:
  1. Se debe mostrar un Modal/Dialog(Vuetify) con los campos: name, username, email y phone.
  2. Validar los siguientes aspectos: Todos los campos son requeridos, "name, username" no acepta números, "email" tiene que ser de formato correo electrónico, "phone" no debe permitir letras.
  3. Al crear un nuevo registro, este registro se debe agregar a la lista almacenada en Vuex para que la Tabla se actualice automáticamente.

  **NOTA:** El modal/dialog debe ser un componente reutilizable y simplemente los botones agregar y editar lo deben llamar.

* Función Editar: :ballot_box_with_check:
  1. Mostrar información del registro a editar en el Modal/Dialog(Vuetify).
  2. Tener las mismas valdiaciones de la función agregar.
  3. Al actualizar el registro, se debe ver reflejado en el State de Vuex. **OJO: :eyes:** Sólo se deben modificar los valores que se encuentran en el formulario.

* Función Eliminar: :heavy_minus_sign:
  1. Eliminar un registro, no hay validación de pormedio.
  2. Se debe ver reflejado en el State de Vuex y posteriormente en la tabla.

Se tomará en cuenta la forma en que plantea la solución, la forma de codificación, la aplicación de conceptos de VueJs. No tengas miedo a equivocarte, de las equivocaciones se aprende. Consulta si hay algo no claro en el reto. :hand: :hand:

Finalmente, se terminará el reto realizando un Merge Request usando la platafoma GitLab.

# :muscle:¡Suerte!:metal: